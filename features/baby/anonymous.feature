@javascript
Feature: Baby traject
When I visit /baby or /bebe
As an anonymous user
I should see the baby landing page in the corresponding language

  Scenario: Anonymous user is redirected from /baby to the Dutch baby landing page
    When I go to "/baby"
    Then the url should match "/nl/baby-aan-boord"
    #And I should get a "200" HTTP response

  Scenario: Anonymous user is redirected from /bebe to the French baby landing page
    When I go to "/bebe"
    Then the url should match "/fr/bebeabord"
    #And I should get a "200" HTTP response

