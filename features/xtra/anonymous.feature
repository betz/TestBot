@javascript
Feature: Xtra Account on colruyt.be
When I visit /login
As an anonymous user
I should see the xtra login form

  Scenario: Anonymous user sees the Xtra login form on /login
    When I go to "/login"
    When I switch to xtra_frame
    Then I should see "Meld je aan met je XTRA-login"

  @smartStep
  Scenario: Anonymous user can create an Xtra account
    When I go to "/login"
    When I click on "#cookienotif > div > a"
    When I hide the brand heading navigation
    When I switch to xtra_frame
    And I click "Registreren"
    Given I sleep 2 seconds
    When I click on "label[for=xtraCard2]"
    When I press "Volgende stap"
    When I cat type a email in "customer.email"
    When I cat type a "password" string in "password"
    When I cat retype a "password" string in "passwordConfirm"
    When I click on "button"
    Given I sleep 2 seconds
    When I switch to xtra_frame
    Then I should see "Vul je klantgegevens in"
    When I click on "#wizardStep2 .radios .radio:first-child span"
    When I cat type a "name_first" string in "customer.firstName"
    When I cat type a "name_last" string in "customer.lastName"
    And I fill in "customer.dateOfBirth.day" with "26"
    And I fill in "customer.dateOfBirth.month" with "11"
    And I fill in "customer.dateOfBirth.year" with "1984"
    And I fill in "customer.address.postalCode" with "1500"
    And I fill in "customer.address.locationName" with "HALLE"
    And I fill in "customer.address.streetName" with "EDINGENSESTEENWEG"
    And I fill in "customer.address.houseNr" with "249"
    And I fill in "customer.address.box" with "1337"
    And I fill in "customer.phones.0.number" with "486123456"
    When I xclick on '//*[@id="wizardStep2"]/div[14]/button[1]'
    Given I sleep 3 seconds
    When I xclick on "//*[@id='wizardStep3']/div[4]/div/label/span[1]"
    When I xclick on "//*[@id='wizardStep3']/div[5]/button[1]"
    Given I sleep 5 seconds
    Then I should see "Bedankt voor je registratie!"
    When I go to "/login"
    When I switch to xtra_frame
    When I log out of Xtra

  @smartStep
  Scenario: Anonymous user can login to Xtra using the last created credentials
    When I go to "/login"
    When I switch to xtra_frame
    When I cat retype a "email" string in "loginName"
    When I cat retype a "password" string in "password"
    And I press "Aanmelden"
    Given I sleep 3 seconds
    When I switch to xtra_frame
    Then I should see "Mijn XTRA-profiel"




