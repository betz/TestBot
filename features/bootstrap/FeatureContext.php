<?php

use Drupal\DrupalExtension\Context\RawDrupalContext;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;
use Behat\MinkExtension\Context\MinkContext;


use Behat\Behat\Context\BehatContext,
    Behat\Behat\Context\Step,
    Behat\Behat\Context\ClosuredContextInterface,
    Behat\Behat\Context\TranslatedContextInterface,
    Behat\Behat\Event\SuiteEvent,
    Behat\Behat\Event\ScenarioEvent,
    Behat\Behat\Hook\Scope\AfterStepScope,
    Behat\Behat\Hook\Scope\AfterScenarioScope,
    Behat\Testwork\Hook\Scope\AfterSuiteScope,
    Behat\Testwork\Hook\Scope\BeforeSuiteScope;


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext {

  protected $password;

  /** @AfterStep */
  public function afterStep(AfterStepScope $scope) {
    date_default_timezone_set('Europe/Brussels');

    $t = microtime(true);
    $micro = sprintf("%06d",($t - floor($t)) * 1000000);
    $d = new DateTime( date('Y-m-d H:i:s.'.$micro, $t) );
    $today = $d->format('Y-m-d');
    $time = $d->format('H.i.s.u');

    $stepname = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $scope->getStep()->getText());
    $path = "screenshots/" . $today;

    if (!file_exists($path)) {
      mkdir($path);
    }

    file_put_contents($path . '/' . $time . ') ' . $stepname . '.png', $this->getSession()->getDriver()->getScreenshot());
  }

  /** @BeforeSuite */
  public static function startTest(BeforeSuiteScope $scope) {
    $data = array(
      "color" => 'green',
      "message_format" => "text",
      "message" => "Aye aye Captain, on it! \nStarting to run your magnificent test suite. \nI'll confirm when I'm done.",
      "notify" => "true",
    );

    $data_string = json_encode($data);
    $ch = curl_init("http://example.com");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );

    #curl_exec($ch);
    curl_close($ch);
  }

  /** @AfterSuite */
  public static function finishTest(AfterSuiteScope $scope) {
    $data = array(
      "color" => 'green',
      "message_format" => "text",
      "message" => "Finished running your test suite, Captain! \nCould I help with something else?",
      "notify" => "true",
    );

    $data_string = json_encode($data);
    $ch = curl_init("http://example.com");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );

    #curl_exec($ch);
    curl_close($ch);

    sleep(5);

    $data = array(
      "color" => 'green',
      "message_format" => "text",
      "message" => "Some rum maybe?",
      "notify" => "true",
    );
    $data_string = json_encode($data);
    $ch = curl_init("http://example.com");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );

    #curl_exec($ch);
    curl_close($ch);
  }

  /** @AfterScenario */
  public static function hipchatlog(AfterScenarioScope $scope) {
    $emoji = ($scope->getTestResult()->isPassed()) ? "❤️" : "😱";
    $status = ($scope->getTestResult()->isPassed()) ? "success" : "fail";
    $color = ($scope->getTestResult()->isPassed()) ? 'green' : 'red';

    $data = array(
      "color" => $color,
      "message_format" => "text",
      "message" => $emoji . "️ " . $status . ": " .  $scope->getScenario()->getTitle(),
      "notify" => "false",
    );

    $data_string = json_encode($data);
    $ch = curl_init("http://example.com");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );

    #curl_exec($ch);
    curl_close($ch);
  }

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct($parameters) {
    $this->cattype = new stdClass();
  }

  /**
   * @When I switch to xtra_frame
   */
  public function switchToXtraFrame() {

    libxml_use_internal_errors(true);
    $doc = new DOMDocument();
    $doc->loadHTML($this->getSession()->getPage()->getContent());
    $iframe = $doc->getElementsByTagName('iframe');

    for ($i = 0; $i < $iframe->length; $i++) {
      $id = $iframe->item($i)->getAttribute('id');
      if(substr( $id, 0, 8 ) === "xtraForm") {
        print('Switching to xtra iframe with ID ' . $id);
        $this->getSession()->switchToIFrame($id);
      }
    }
  }

  /**
   * @When I switch to parent frame
   */
  public function switchToParentFrame() {
    $this->getSession()->switchToIFrame();
  }

  /**
   * @When I hide the brand heading navigation
   */
  public function hideBrandHeading() {
    $this->getSession()->executeScript("document.getElementsByClassName('main-navigation__brand-heading')[0].style.display = 'none';");
  }

  /**
   * Fills in random email in a form field with specified id|name|label|value
   *
   * @When I cat type a email in :arg1
   */
  public function iCatTypeEmail($field) {
    global $cattype;
    $chr = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < 6; $i++) {
      $randomString .= $chr[rand(0, strlen($chr) - 1)];
    }

    $email = "testbotuser.{$randomString}@example.com";
    $cattype['email'] = $email;
    $this->getSession()->getPage()->fillField($field, $email);
  }

  /**
   * Fills in random string in a form field with specified id|name|label|value
   *
   * @When I cat type a :arg1 string in :arg2
   */
  public function iCatTypeString($type, $field) {
    global $cattype;
    $chr = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < 8; $i++) {
      $randomString .= $chr[rand(0, strlen($chr) - 1)];
    }

    $cattype[$type] = $randomString;
    $this->getSession()->getPage()->fillField($field, $randomString);
  }

  /**
   * Fills in random string in a form field with specified id|name|label|value
   *
   * @When I cat retype a :arg1 string in :arg2
   */
  public function iCatTypeLastString($type, $field) {
    global $cattype;
    print_r($cattype);
    $this->getSession()->getPage()->fillField($field, $cattype[$type]);
  }

  /**
   * @Given I manually press :key
   */
  public function manuallyPress($key)
  {
    $script = "$( \"#target\" ).keydown();";
    $this->getSession()->evaluateScript($script);
  }

  /**
   * Sleeps X seconds
   * Example: Given I sleep 2 seconds
   *
   * @Given I sleep :arg1 seconds
   */
  public function iSleep($seconds) {
    sleep($seconds);
  }

  /**
   * @Then /^I click on "([^"]*)"$/
   */
  public function iClickOn($element) {
    $page = $this->getSession()->getPage();
    $findName = $page->find("css", $element);
    if (!$findName) {
      throw new Exception($element . " could not be found");
    } else {
      $findName->click();
    }
  }

  /**
   * @Then I xclick on :Xelement
   */
  public function iXClickOn($xelement) {
    $page = $this->getSession()->getPage();
    $findName = $page->find("xpath", $xelement);
    if (!$findName) {
      throw new Exception($xelement . " could not be found");
    } else {
      $findName->click();
    }
  }

  /**
   * @Then I log out of Xtra
   */
  public function iLogOutXtra() {
    $page = $this->getSession()->getPage();
    $findName = $page->find("css", "a[xtra-type='logoff']");
    if (!$findName) {
      throw new Exception($element . " could not be found");
    } else {
      $findName->click();
    }
  }

  /**
   * @Then I should see more then :amount of :elements
   */
  public function iSeeMoreThenElements($amount, $element) {
    $page = $this->getSession()->getPage();
    $find = $page->find("css", $element);

    $dom = new DomDocument();
    $dom->loadHTML($find->getParent()->getHtml());

    $finder = new DomXPath($dom);
    $classname = "views-row";
    $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");

    if($nodes->length <= $amount) {
      throw new Exception('not enough elements: ' . $nodes->length);
    }
  }

  /**
   * @Then I should exactly :amount of :elements
   */
  public function iSeeExactlyElements($amount, $element) {
    $page = $this->getSession()->getPage();
    $find = $page->find("css", $element);

    $dom = new DomDocument();
    $dom->loadHTML($find->getParent()->getHtml());

    $finder = new DomXPath($dom);
    $classname = "views-row";
    $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");

    if($nodes->length != $amount) {
      throw new Exception('not the correct amount of elements:' . $nodes->length);
    }
  }

  /**
   * Wait until the id="updateprogress" element is gone,
   * or timeout after 3 minutes (180,000 ms).
   * See https://swsblog.stanford.edu/blog/behat-custom-step-definition-wait-batch-api-finish
   *
   * @Given I wait for the batch job to finish
   */
  public function iWaitForTheBatchJobToFinish() {
    $this->getSession()->wait(180000, 'jQuery("#updateprogress").length === 0');
  }
}
