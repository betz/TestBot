<?php

use Drupal\DrupalExtension\Context\RawDrupalContext;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Tester\Exception\PendingException;

use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Context\Step;
use Behat\Behat\Context\ClosuredContextInterface,
  Behat\Behat\Context\TranslatedContextInterface,
  Behat\Behat\Context\BehatContext;
use Behat\Behat\Hook\Scope\AfterStepScope;
use MinkFieldRandomizer\Context\FieldRandomizerTrait;

/**
 * Defines application features from the specific context.
 */
class DummyContext extends RawDrupalContext {

}
