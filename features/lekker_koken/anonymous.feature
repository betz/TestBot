@javascript
Feature: Lekker koken
When I visit the 'uitgebreid zoeken page'
As an anonymous user
I should be able to search for recipes

  Scenario: Anonymous user gets multiple recipes on the 'uitgebreid zoeken' page
    When I go to "/nl/lekker-koken/uitgebreid-zoeken/recepten"
    Then I should see more then "1" of ".view-id-recipe-search .views-row"
