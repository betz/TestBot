# TestBot
A all-in-one test suite for all you testing cravings. Batteries included.

## Features
* Behat / Mink / Selenium
* Containerized with Docker
* Screenshots per step
* Bot notifies a Hipchat room
* MQTT client / listens to 'start' command
* node/npm included

## Instructions

### Prerequisites
- install composer (https://getcomposer.org/download)
- install docker (https://docs.docker.com/install)
- install docker-compose (https://docs.docker.com/compose/install)

### First install
- run `composer install` to install php requirements
  - this will install all requirements in ./vendor and ./bin
- run docker-compose: `docker-compose up` to build the docker containers
  - this will install the containers and keep running

### Run tests
- While running `docker-compose up`, run `./run-behat` to run all tests

## Interesting commands
- Run all test suites
  - `./run-behat`
- Run specific tests
  - `./run-behat features/lekker_koken`
- Get overview of available gherkin steps for using in scenarios
  - `TODO`
  - 
  
## Notes
- The containerized behat suite was build on topof https://github.com/docksal/behat
